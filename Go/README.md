## GO API_Client

适用于 UP控制台8.x版本

### Install

  `go get -v gitee.com/i2soft/api-client/Go`

### Demo

```
package main

import (
	"fmt"

	upclient "gitee.com/i2soft/api-client/Go/httpclient"
)

func main() {
	ip := "172.20.68.203"
	port := "58086"
	ak := "ssss" // 登录控制台，个人信息页获取
	sk := "dddd" // 只在创建时出现一次
	client := upclient.NewHttpClient(ip, port, ak, sk)

	rs, err := client.DoRequest("GET", "/node", map[string]interface{}{})
	if err != nil {
		fmt.Println("err", err)
	} else {
		fmt.Println("resp", rs)
	}
}
```

  [更多](./httpclient/httpclient_test.go)