package httpclient

import (
	"fmt"
	"testing"
)

func TestCliet(t *testing.T) {
	// Instantiate the HttpClient
	client := NewHttpClient("172.20.2.10", "58086", "HnQScq9eRCf1FPDdrv0sKikoO2E68Vp3", "epv715nkmHdi2GRxqsYVKTWcSgfyjbCFLM0E8J3Q")

	// Define the service endpoint and parameters
	service := "/node/auth"
	//paramsGet := map[string]interface{}{}

	//paramsPost := map[string]interface{}{"param1": "value1", "param2": "value2"}
	//paramsPost := map[string]interface{}{
	//	"param1": "value1",
	//	"param2": map[string]interface{}{
	//		"param2-1": 123456,
	//		"param2-2": "value2-2中文",
	//	},
	//}

	//paramsPost := map[string]interface{}{
	//	"param1": "value1",
	//	"param2": map[string]interface{}{
	//		"param2-1": 123456,
	//		"param2-2": map[string]interface{}{
	//			"param2-2-1": "value2-2-1中文",
	//		},
	//	},
	//}

	paramsPost := map[string]interface{}{
		"config_addr":    "192.168.254.136",
		"config_port":    "26301",
		"os_pwd":         "vm.ROOT123",
		"os_user":        "root",
		"proxy_switch":   0,
		"node_uuid":      "",
		"i2id":           "",
		"use_credential": 0,
		"cred_uuid":      "",
		"is_ssl":         1,
	}

	//// Make a GET request
	//responseGet, errGet := client.DoRequest("GET", service, paramsGet)
	//if errGet != nil {
	//	fmt.Println("GET Request error:", errGet)
	//	return
	//}
	//fmt.Println("GET Response:", responseGet)

	// Make a POST request
	responsePost, errPost := client.DoRequest("POST", service, paramsPost)
	if errPost != nil {
		fmt.Println("POST Request error:", errPost)
		return
	}
	fmt.Println("POST Response:", responsePost)
}
