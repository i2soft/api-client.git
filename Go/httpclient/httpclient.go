package httpclient

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"sort"
	"strings"
	"time"
)

type stringMap map[string]interface{}

type HttpClient struct {
	ip      string
	ak      string
	sk      string
	apiRoot string
	headers stringMap
	client  *http.Client
}

// Constructor for HttpClient
func NewHttpClient(ip, port, ak, sk string) *HttpClient {
	// Create a custom http.Client with InsecureSkipVerify
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := &http.Client{Transport: tr}

	return &HttpClient{
		ip:      ip,
		ak:      ak,
		sk:      sk,
		apiRoot: fmt.Sprintf("https://%s:%s/api", ip, port),
		headers: make(stringMap),
		client:  httpClient,
	}
}

// Method to make an HTTP request
func (client *HttpClient) DoRequest(method, service string, params map[string]interface{}) (map[string]interface{}, error) {
	fullUrl := client.apiRoot + service

	var req *http.Request
	var err error

	if params != nil {
		err := client.addSignToHeader(method, fullUrl, params)
		if err != nil {
			return nil, err
		}

		if method == "GET" {
			formData := url.Values{}
			for key, value := range params {
				formData.Add(key, fmt.Sprintf("%v", value))
			}
			req, err = http.NewRequest(method, fullUrl+"?"+formData.Encode(), nil)
		} else {
			temp := buildJsonBody(params)
			req, err = http.NewRequest(method, fullUrl, strings.NewReader(temp))
			req.Header.Set("Content-Type", "application/json")
		}
		for key, value := range client.headers {
			req.Header.Set(key, fmt.Sprintf("%v", value))
		}

		if err != nil {
			return nil, fmt.Errorf("error creating request: %v", err)
		}
	} else {
		req, err = http.NewRequest(method, fullUrl, nil)
		if err != nil {
			return nil, fmt.Errorf("error creating request: %v", err)
		}
	}

	resp, err := client.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error making request: %v", err)
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing response body: %v\n", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %v", resp.StatusCode)
	}

	var result map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("error reading response body: %v", err)
	}

	return result, nil
}

func (sm stringMap) removeEmptyValue() stringMap {
	newMap := make(stringMap)
	for k, v := range sm {
		if v != nil && v != "" {
			newMap[k] = v
		}
	}
	return newMap
}

func (sm stringMap) formString() string {
	values := url.Values{}
	for k, v := range sm {
		values.Set(k, fmt.Sprintf("%v", v))
	}
	return values.Encode()
}

func (client *HttpClient) addSignToHeader(httpMethod, urlStr string, args stringMap) error {
	apiIndex := strings.Index(urlStr, "/api/")
	if apiIndex == -1 {
		return nil
	}
	api := urlStr[apiIndex:]

	// 如果是以下2个接口，跳过签名
	noneAuth := map[string]struct{}{
		"/api/auth/token":          {},
		"/api/sys/public_settings": {},
	}

	if _, exists := noneAuth[api]; exists {
		return nil
	}

	// 准备签名用的字符串
	randomStr := getRandomString(16)
	timeStr := fmt.Sprintf("%d", time.Now().Unix())
	uuid := uuid.New().String()
	signData := fmt.Sprintf("%s\n%s\n%s\n%s\n%s", strings.ToUpper(httpMethod), api, randomStr, timeStr, uuid)
	hash, err := generateHMACSHA256(client.sk, signData)
	if err != nil {
		return err
	}

	client.headers["ACCESS-KEY"] = client.ak
	client.headers["timestamp"] = timeStr
	client.headers["nonce"] = uuid
	client.headers["Signature"] = hash

	args["_"] = randomStr // 签名必备随机串

	var signField string
	if httpMethod == "GET" {
		newArgs := args.removeEmptyValue() // 去除空值
		newArgs = sortByKey(newArgs)       // 重新排序
		signField = newArgs.formString()   // 构建url参数
	} else {
		keys := make([]string, 0, len(args))
		for k := range args {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		sb := strings.Builder{}
		for _, key := range keys {
			// 判断 value 是否为 string
			value := args[key]
			if _, ok := value.(string); !ok {
				if value == nil {
					continue
				}

				// 将非字符串的值编码为 JSON
				jsonValue, err := json.Marshal(value)
				if err != nil {
					continue
				}
				value = string(jsonValue)

				valueStr := value.(string)

				// 属性内的 {} 改为 []
				//valueStr = strings.ReplaceAll(valueStr, "{", "[")
				//valueStr = strings.ReplaceAll(valueStr, "}", "]")
				//if valueStr == "{}" {
				//	valueStr = "[]"
				//}

				// 去除双引号
				valueStr = strings.ReplaceAll(valueStr, "\"", "")

				value = valueStr
			}

			// 跳过空字符串的值
			if reflect.ValueOf(value).String() == "" {
				continue
			}

			// 构建 signField 字符串
			sb.WriteString(fmt.Sprintf("%s=%v&", key, value))
		}

		signField = sb.String()
		// 删除最后一个多余的 "&"
		signField = strings.TrimRight(signField, "&")
	}
	enhanceStr, err := generateHMACSHA256(client.sk, signField)
	if err != nil {
		return err
	}

	client.headers["enhanceStr"] = strings.ToLower(enhanceStr)
	return nil
}

func buildJsonBody(args stringMap) string {
	var buf bytes.Buffer
	encoder := json.NewEncoder(&buf)
	encoder.SetEscapeHTML(false) // 类似于 JSON_UNESCAPED_SLASHES
	if err := encoder.Encode(args); err != nil {
		fmt.Println("Error encoding JSON:", err)
		return ""
	}
	return buf.String()
}

func getRandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func generateHMACSHA256(secret, data string) (string, error) {
	h := hmac.New(sha256.New, []byte(secret))
	_, err := h.Write([]byte(data))
	if err != nil {
		return "", err
	}
	hash := h.Sum(nil)
	resultStr := strings.ToLower(hex.EncodeToString(hash))
	return resultStr, nil
}

func sortByKey(args stringMap) stringMap {
	keys := make([]string, 0, len(args))
	for k := range args {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	sortedMap := make(stringMap)
	for _, k := range keys {
		sortedMap[k] = args[k]
	}
	return sortedMap
}
